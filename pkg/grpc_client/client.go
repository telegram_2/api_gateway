package grpc_client

import (
	"fmt"

	"gitlab.com/telegram_2/api_gateway/config"
	pbu "gitlab.com/telegram_2/api_gateway/genproto/user_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

//go:generate mockgen -source ../../genproto/user_service/user_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/user_service_grpc.gen.go
//go:generate mockgen -source ../../genproto/user_service/auth_service_grpc.pb.go -package mock_grpc -destination ./mock_grpc/auth_service_grpc.gen.go

type GrpcClientI interface {
	UserService() pbu.UserServiceClient
	SetUserService(u pbu.UserServiceClient)
	AuthService() pbu.AuthServiceClient
	SetAuthService(u pbu.AuthServiceClient)
}

type GrpcClient struct {
	cfg         config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %v port: %v", cfg.UserServiceHost, cfg.UserServiceGrpcPort)
	}

	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service":     pbu.NewUserServiceClient(connUserService),
			"auth_service":     pbu.NewAuthServiceClient(connUserService),
		},
	}, nil
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) AuthService() pbu.AuthServiceClient {
	return g.connections["auth_service"].(pbu.AuthServiceClient)
}


