FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /api

COPY . .

RUN go build -o main cmd/main.go

FROM alpine:3.16

WORKDIR /api

COPY --from=builder /api/main .

EXPOSE 8080

CMD [ "/api/main" ]
